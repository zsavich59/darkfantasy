// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"

#include "Enemy.h"
#include "Components/BoxComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Particles/ParticleSystemComponent.h"
#include "MainCharacter.h"
#include "Engine/StaticMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Niagara/Public/NiagaraFunctionLibrary.h"
#include "Sound/SoundCue.h"

class AEnemy;
// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	DefaultScene = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultComponent"));
	RootComponent = DefaultScene;

	// Created Static Mesh Component
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(GetRootComponent());
	Mesh->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	Mesh->SetSimulatePhysics(false);
	
	// Created Box Collision that will damage enemy
	CombatCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("CombatCollision"));
	CombatCollision->SetupAttachment(GetRootComponent());
	CombatCollision->SetCollisionObjectType(ECC_WorldDynamic);
	CombatCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	CombatCollision->SetCollisionResponseToChannel(ECC_Pawn,ECR_Overlap);
	CombatCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision); // Set this NoCollision

	// Created Particle Component for Extra Attacks when energy is 100%
	ExtraAttackParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ExtraAttackParticle"));
	ExtraAttackParticle->SetupAttachment(GetRootComponent());

	// VFX
	NiagaraSword = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NiagaraSword"));
	NiagaraSword->SetupAttachment(GetRootComponent());
	NiagaraSword->Deactivate();
	
	// Weapon Stats
	MinDamage = 10;
	MaxDamage = 25;
	bCanExtraParticles = false;
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();

	CombatCollision->OnComponentBeginOverlap.AddDynamic(this,&AWeapon::CombatBeginOverlap);
}

// Called every frame
void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeapon::CombatBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor != WeaponOwner && WeaponInstigator && DamageType)
	{
		int32 const Damage = FMath::RandRange(MinDamage, MaxDamage);
		DamagedActor = OtherActor;
		UGameplayStatics::ApplyDamage(OtherActor,Damage,WeaponInstigator,this,DamageType);
		if(HitNiagara && bCanExtraParticles)
		{
			FVector SocketLocation =  Mesh->GetSocketLocation("S_Hit");
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),HitNiagara,SocketLocation,FRotator(0.f),FVector(0.05f));
		}
	}
}

void AWeapon::EquipWeapon(ACharacter* Pawn)
{
	if(Pawn)
	{
		SetInstigator(Pawn->GetController());
		
		if(WeaponInstigator)
		{
			WeaponOwner = WeaponInstigator->GetCharacter();

			if(WeaponOwner)
			{
				const USkeletalMeshSocket* WeaponSocket = Pawn->GetMesh()->GetSocketByName("S_Sword");
				if(WeaponSocket)
				{
					WeaponSocket->AttachActor(this, Pawn->GetMesh());
				}
			}
		}
	}
}

void AWeapon::ActivateCollision()
{
	CombatCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AWeapon::DeactivateCollision()
{
	CombatCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AWeapon::ActivateShining()
{
	if(NiagaraSword && ShiningSwordMaterial)
	{
		bCanExtraParticles = true;
		NiagaraSword->Activate();
		Mesh->SetMaterial(0,ShiningSwordMaterial);
	}
}

void AWeapon::PlayHitSounds()
{
	if(HitSound)
	{
		UGameplayStatics::PlaySound2D(this,HitSound);
	}
}

