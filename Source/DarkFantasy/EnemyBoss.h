// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enemy.h"
#include "EnemyBoss.generated.h"

/**
 * 
 */
UCLASS()
class DARKFANTASY_API AEnemyBoss : public AEnemy
{
	GENERATED_BODY()
	
	public:
	AEnemyBoss();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Boss")
	bool bIsReady;
	

	virtual void Tick(float DeltaSeconds) override;
	virtual void AgroCollisionOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	virtual void CombatCollisionOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	virtual void Hit() override;
};
