// Copyright Epic Games, Inc. All Rights Reserved.

#include "DarkFantasy.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DarkFantasy, "DarkFantasy" );
