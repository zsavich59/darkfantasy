// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacter.h"

#include "Enemy.h"
#include "Weapon.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"

class AEnemy;
// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set Base Rates for Turn and Look
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Set Height For Camera Boom
	CameraHeight = 400.f;
	CameraHeightMin = 300.f;
	CameraHeightMax = 500.f;
	bSmoothCameraHeight = false;

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	//Movement Props
	NormalWalkSpeed = 200.f;
	SprintingWalkSpeed = 375.f;
	MovementStatus = EMovementStatus::EMS_Normal;
	StaminaStatus = EStaminaStatus::ESS_Normal;
	bIsTurnLeft = false;
	bIsTurnLeft = false;

	// Setup Character Movements
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->MaxWalkSpeed = NormalWalkSpeed;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 540.f, 0.f);
	GetCharacterMovement()->AirControl = 0.2f;

	// Create CameraBoom Component
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = CameraHeight;
	CameraBoom->bUsePawnControlRotation = true;
	CameraBoom->bEnableCameraLag = true; 
	CameraBoom->CameraLagSpeed = 6.f;

	// Create FollowCamera Component
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	// Setup NoCollision for Main Skeletal Mesh Component
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// Combat Props
	bIsAttacking = false;
	bIsHitting = false;

	// Character Stats
	Health = 100;
	MaxHealth = 100;

	Stamina = 100;
	MaxStamina =  100;
	MinBorderStamina = MaxStamina * 0.3;

	Energy = 0;
	MaxEnergy = 100;
	
	
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();
	SpawnWeapon();
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if(MovementStatus == EMovementStatus::EMS_Dead) return;
	
	// Camera Smooth Functional
	if(bSmoothCameraHeight)
	{
		CameraBoom->TargetArmLength = FMath::FInterpTo(CameraBoom->TargetArmLength,CameraHeight, DeltaTime, 2.f);;
		if(FMath::IsNearlyEqual(CameraBoom->TargetArmLength, CameraHeight, 1.f)) bSmoothCameraHeight = false;
	}

	// Checking Stamina Status
	if(bShiftKeyDown == true && MovementStatus == EMovementStatus::EMS_Sprinting)
	{
		DecreaseStamina(10 * DeltaTime);
	}
	if(bShiftKeyDown == false && StaminaStatus == EStaminaStatus::ESS_Exhausted)
	{
		SetStaminaStatus(EStaminaStatus::ESS_Recovering);
	}
	if(bShiftKeyDown == false && Stamina < MaxStamina || StaminaStatus == EStaminaStatus::ESS_Recovering)
	{
		IncreaseStamina(10* DeltaTime);
	}
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Movement inputs
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AMainCharacter::ShiftKeyDown);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AMainCharacter::ShiftKeyUp);
	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &AMainCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &AMainCharacter::LookUpAtRate);

	// Mouse inputs
	PlayerInputComponent->BindAction("LMB", IE_Pressed, this, &AMainCharacter::LeftMouseButton);
	PlayerInputComponent->BindAction("RMB", IE_Pressed, this, &AMainCharacter::RightMouseButton);
	PlayerInputComponent->BindAxis("MouseWheel", this, &AMainCharacter::UpdateCameraHeight);
}

// Called when character take damage
float AMainCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	if(MovementStatus != EMovementStatus::EMS_Dead)
	{
		Hit();
		DecreaseHealth(DamageAmount);
	}
	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void AMainCharacter::MoveForward(float Value)
{
	if((Controller != nullptr) && (Value != 0) && MovementStatus != EMovementStatus::EMS_Dead)
	{
		bIsMoveForward = true;
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.f, Rotation.Yaw, 0.f);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
		return;
	}
	if(bIsMoveForward) bIsMoveForward = false;
}

void AMainCharacter::MoveRight(float Value)
{
	if((Controller != nullptr) && (Value != 0) && MovementStatus != EMovementStatus::EMS_Dead)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0.f, Rotation.Yaw, 0.f);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void AMainCharacter::TurnAtRate(float Rate)
{
	if(Rate != 0 && MovementStatus != EMovementStatus::EMS_Dead && !bIsAttacking)
	{
		if(!bIsMoveForward && (Rate < -0.2 || Rate > 0.2))
		{
			bIsTIP = true;
			if(Rate < 0) bIsTurnLeft = true;
		}
		else bIsTIP = false;
		
		AddControllerYawInput(Rate);
		return;
	}
	if(bIsTurnLeft || bIsTIP)
	{
		bIsTIP = false;
		bIsTurnLeft = false;
	}
}

void AMainCharacter::LookUpAtRate(float Rate)
{
	if(MovementStatus != EMovementStatus::EMS_Dead)	AddControllerPitchInput(Rate);
}

void AMainCharacter::ShiftKeyDown()
{
	bShiftKeyDown = true;
	if(MovementStatus != EMovementStatus::EMS_Dead)
	{
		if(StaminaStatus != EStaminaStatus::ESS_Exhausted && StaminaStatus != EStaminaStatus::ESS_Recovering)
		{
			SetMovementStatus(EMovementStatus::EMS_Sprinting);
		}
	}
}

void AMainCharacter::ShiftKeyUp()
{
	bShiftKeyDown = false;
	if(MovementStatus != EMovementStatus::EMS_Dead)
	{
		SetMovementStatus(EMovementStatus::EMS_Normal);
	}
}

void AMainCharacter::UpdateCameraHeight(float Value)
{
	if(Value != 0)
	{
		if((CameraHeight + Value * 25.f > CameraHeightMax) || (CameraHeight + Value * 25.f < CameraHeightMin)) return;
		CameraHeight += Value * 25.f;
		bSmoothCameraHeight = true;
	}
	
}

void AMainCharacter::SetMovementStatus(EMovementStatus Status)
{
	MovementStatus = Status;
	if(Status == EMovementStatus::EMS_Normal)
	{
		GetCharacterMovement()->MaxWalkSpeed = NormalWalkSpeed;
	}
	else if (Status == EMovementStatus::EMS_Sprinting)
	{
		GetCharacterMovement()->MaxWalkSpeed = SprintingWalkSpeed;
	}
}

void AMainCharacter::SetStaminaStatus(EStaminaStatus Status)
{
	StaminaStatus = Status;	
}

void AMainCharacter::JumpBack()
{
	if(!bIsAttacking)
	{
		bSKeyJustUp = false;
		PlayMontage(CombatMontage, "Evade_Back", 1.5f);
	}
}

void AMainCharacter::LeftMouseButton()
{
	if(!bIsHitting)
	{
		FastAttack();
	}
}

void AMainCharacter::RightMouseButton()
{
	if(!bIsHitting)
	{
		LethalAttack();
	}
}

void AMainCharacter::FastAttack()
{
	if(CanAttack())
	{
		bIsAttacking = true;
		PlayMontage(CombatMontage, "Slash_01", "Slash_02", "Slash_03");
	}
}

void AMainCharacter::LethalAttack()
{
	if(CanAttack()/* && Energy == MaxEnergy*/)
	{
		TArray<AActor*> OverlappingActors;
		GetOverlappingActors(OverlappingActors, AEnemy::StaticClass());
		if(OverlappingActors.Num() == 0) return;

		for(auto const Actor : OverlappingActors)
		{
			AEnemy* Enemy = Cast<AEnemy>(Actor);
			float Distance = (GetActorLocation() - Actor->GetActorLocation()).Size();
			UE_LOG(LogTemp, Warning, TEXT("Distance = %f"), Distance);
			if(Enemy && Distance <= 250.f)
			{
				UE_LOG(LogTemp, Warning, TEXT("Lethal_01") );
				Enemy->LethalDie("Lethal_01");				
			}
		}
		
		bIsAttacking = true;
		PlayMontage(CombatMontage, "Lethal_01");
		SetEnergy(0.f);
	}
}

bool AMainCharacter::CanAttack()
{
	return !bIsAttacking && MovementStatus != EMovementStatus::EMS_Sprinting && MovementStatus != EMovementStatus::EMS_Dead;
}

void AMainCharacter::Hit()
{
	if(!bIsAttacking)
	{
		bIsHitting = true;
	}
}

void AMainCharacter::Die()
{
	SetMovementStatus(EMovementStatus::EMS_Dead);
	PlayMontage(CombatMontage, "Death_01", "Death_02");
}

void AMainCharacter::IncreaseHealth(float const Value)
{
	if(Health + Value >= MaxHealth) Health = MaxHealth;
	else Health += Value;
}

void AMainCharacter::DecreaseHealth(float const Value)
{
	if(Health - Value <= 0) {Health = 0; Die();}
	else Health -= Value;
}

void AMainCharacter::IncreaseStamina(float const Value)
{
	if(Stamina + Value > MinBorderStamina) SetStaminaStatus(EStaminaStatus::ESS_Normal);
	
	if(Stamina + Value >= MaxStamina)
	{
		Stamina = MaxStamina;
	}
	else Stamina += Value;
}

void AMainCharacter::DecreaseStamina(float const Value)
{
	if(Stamina - Value <= 0)
	{
		Stamina = 0;
		SetMovementStatus(EMovementStatus::EMS_Normal);
		SetStaminaStatus(EStaminaStatus::ESS_Exhausted);
	}
	else Stamina -= Value;
}

void AMainCharacter::IncreaseEnergy(float const Value)
{
	if(Energy + Value >= MaxEnergy) Energy = MaxEnergy;
	else Energy += Value;
}

void AMainCharacter::DecreaseEnergy(float const Value)
{
	if(Energy - Value <= 0) Energy = 0;
	else Energy -= Value;
}

void AMainCharacter::SpawnWeapon()
{
	if(SelectEquipedWeapon)
	{
		EquipedWeapon = GetWorld()->SpawnActor<AWeapon>(SelectEquipedWeapon);
		if(EquipedWeapon)
		{
			EquipedWeapon->EquipWeapon(this);
		}
	}
}

void AMainCharacter::PlayMontage(UAnimMontage* Montage, FName Section, float Speed)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(AnimInstance && Montage)
	{
		AnimInstance->Montage_Play(Montage, Speed);
		AnimInstance->Montage_JumpToSection(Section);
	}
}

void AMainCharacter::PlayMontage(UAnimMontage* Montage, FName Section1, FName Section2)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	
	if(AnimInstance && Montage)
	{
		int32 Select = FMath::RandRange(0,1);
		switch(Select)
		{
			case 0:
				AnimInstance->Montage_Play(Montage, 1.f);
				AnimInstance->Montage_JumpToSection(Section1);
				break;
			case 1:
				AnimInstance->Montage_Play(Montage, 1.f);
				AnimInstance->Montage_JumpToSection(Section2);
				break;
			default:
				;
		}
	}
}

void AMainCharacter::PlayMontage(UAnimMontage* Montage, FName Section1, FName Section2, FName Section3)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	
	if(AnimInstance && Montage)
	{
		int32 Select = FMath::RandRange(0,2);
		switch(Select)
		{
		case 0:
			AnimInstance->Montage_Play(Montage, 1.f);
			AnimInstance->Montage_JumpToSection(Section1);
			break;
		case 1:
			AnimInstance->Montage_Play(Montage, 1.f);
			AnimInstance->Montage_JumpToSection(Section2);
			break;
		case 2:
			AnimInstance->Montage_Play(Montage, 1.f);
			AnimInstance->Montage_JumpToSection(Section3);
			break;
		default:
			;
		}
	}
}

void AMainCharacter::DeathEnd()
{
	GetMesh()->bPauseAnims = true;
	GetMesh()->bNoSkeletonUpdate = true;
}

void AMainCharacter::SetHealth(float const Value)
{
	if(Value >= MaxHealth)
	{
		Health = MaxHealth;
		return;
	}
	Health = Value;
}

void AMainCharacter::SetStamina(float const Value)
{
	if(Value >= MaxStamina)
	{
		Stamina = MaxStamina;
		return;
	}
	Stamina = Value;
}

void AMainCharacter::SetEnergy(float const Value)
{
	if(Value >= MaxEnergy)
	{
		Energy = MaxEnergy;
		return;
	}
	Energy = Value;
}
