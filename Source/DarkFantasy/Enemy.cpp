// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"

#include "AIController.h"
#include "MainCharacter.h"
#include "Weapon.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Sound/SoundCue.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup Components
	AgroCollision = CreateDefaultSubobject<USphereComponent>(TEXT("AgroCollision"));
	AgroCollision->SetupAttachment(GetMesh());
	AgroCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	AgroCollision->SetCollisionObjectType(ECC_WorldDynamic);
	AgroCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	AgroCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	AgroCollision->SetSphereRadius(400.f);

	CombatCollision = CreateDefaultSubobject<USphereComponent>(TEXT("CombatCollision"));
	CombatCollision->SetupAttachment(GetMesh());
	CombatCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CombatCollision->SetCollisionObjectType(ECC_WorldDynamic);
	CombatCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	CombatCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	CombatCollision->SetSphereRadius(100.f);

	/*WeaponCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("WeaponCollision"));
	WeaponCollision->SetupAttachment(GetMesh());
	WeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponCollision->SetCollisionObjectType(ECC_WorldDynamic);
	WeaponCollision->SetCollisionResponseToAllChannels(ECR_Ignore);
	WeaponCollision->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);*/

	GetCharacterMovement()->MaxWalkSpeed = 180.f;

	// Enemy Stats
	SetMovementStatus(EEnemyMovementStatus::EMS_Patrol);
	Health = 100;
	MaxHealth = 100;
	MinDamage = 5;
	MaxDamage = 15;
	MinAttackDelay = 0.5f;
	MaxAttackDelay = 1.5f;
	bIsCriticalStatus = false;

}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	AI = Cast<AAIController>(GetController());

	AgroCollision->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::AgroCollisionOnBeginOverlap);
	AgroCollision->OnComponentEndOverlap.AddDynamic(this, &AEnemy::AgroCollisionOnEndOverlap);

	CombatCollision->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::CombatCollisionOnBeginOverlap);
	CombatCollision->OnComponentEndOverlap.AddDynamic(this, &AEnemy::CombatCollisionOnEndOverlap);

	GetMesh()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);

	SpawnWeapon();
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float AEnemy::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	if(MovementStatus == EEnemyMovementStatus::EMS_Dead) return 0;
	
	DecreaseHealth(DamageAmount);
	
	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void AEnemy::AgroCollisionOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                         UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor)
	{
		AMainCharacter* Character = Cast<AMainCharacter>(OtherActor);
		if(Character)
		{
			SetTarget(Character);
			if(bCanScream)
			{
				PlayScream();
				return;
			}
			MoveToTarget();
		}
	}
}

void AEnemy::AgroCollisionOnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(OtherActor == Target)
	{
		ClearTarget();
		if(AI) AI->StopMovement();
		SetMovementStatus(EEnemyMovementStatus::EMS_Patrol);
	}
}

void AEnemy::CombatCollisionOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor)
	{
		AMainCharacter* Character = Cast<AMainCharacter>(OtherActor);
		if(Character && MovementStatus != EEnemyMovementStatus::EMS_Dead)
		{
			Attack();
		}
	}
}

void AEnemy::CombatCollisionOnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(MovementStatus == EEnemyMovementStatus::EMS_Dead) return;
	
	if(MovementStatus != EEnemyMovementStatus::EMS_Attack)
	{
		GetWorldTimerManager().ClearTimer(AttackDelayTimer);
		MoveToTarget();	
	}
	else if (MovementStatus ==EEnemyMovementStatus::EMS_Attack)
	{
		SetMovementStatus(EEnemyMovementStatus::EMS_MoveTo);
	}
}

void AEnemy::MoveToTarget()
{
	if(MovementStatus == EEnemyMovementStatus::EMS_Dead) return;
	
	SetMovementStatus(EEnemyMovementStatus::EMS_MoveTo);
	if(AI && Target)
	{
		AI->MoveToActor(Target, 25.f);
	}
}

void AEnemy::SpawnWeapon()
{
	if(SelectEquipedWeapon)
	{
		EquipedWeapon = Cast<AWeapon>(GetWorld()->SpawnActor(SelectEquipedWeapon));
		if(EquipedWeapon)
		{
			EquipedWeapon->EquipWeapon(this);
		}
	}
}

void AEnemy::Attack()
{
	if(MovementStatus == EEnemyMovementStatus::EMS_Dead) return;
	UE_LOG(LogTemp, Warning, TEXT("AEnemy::Attack"));
	SetMovementStatus(EEnemyMovementStatus::EMS_Attack);
	PlayMontage(CombatMontage, "Attack_01", "Attack_02", "Attack_03");
}

void AEnemy::AttackEnd()
{
	if(MovementStatus == EEnemyMovementStatus::EMS_Attack)
	{
		SetMovementStatus(EEnemyMovementStatus::EMS_MoveTo);
		float AttackDelay = FMath::RandRange(MinAttackDelay, MaxAttackDelay);
		GetWorldTimerManager().SetTimer(AttackDelayTimer,this,&AEnemy::Attack,AttackDelay);
	} else if (MovementStatus == EEnemyMovementStatus::EMS_MoveTo)
	{
		MoveToTarget();
	}
}

void AEnemy::LethalDie(FName LethalName)
{
	if(MovementStatus == EEnemyMovementStatus::EMS_Dead) return;
	
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(AnimInstance && CombatMontage)
	{
		UE_LOG(LogTemp, Warning, TEXT("AEnemy::LethalDie"));
		AnimInstance->Montage_Stop(1, CombatMontage);
		AnimInstance->Montage_Play(CombatMontage);
		AnimInstance->Montage_JumpToSection(LethalName);
	}
	SetMovementStatus(EEnemyMovementStatus::EMS_Dead);
}

void AEnemy::DecreaseHealth(float const Damage)
{
	if (Health - Damage <= 0)
	{
		Health = 0;
		Die();
	} else
	{
		Health -= Damage;
		Hit();
	}
}

void AEnemy::Die()
{
	if(Target)
	{
		Target->IncreaseEnergy(25.f);
	}
	SetMovementStatus(EEnemyMovementStatus::EMS_Dead);
	PlayMontage(CombatMontage, "Death_01", "Death_02");
}

void AEnemy::Hit()
{
	if(MovementStatus == EEnemyMovementStatus::EMS_Dead) return;
	
	if(MovementStatus != EEnemyMovementStatus::EMS_Attack)
	{
		UE_LOG(LogTemp, Warning, TEXT("AEnemy::Hit"));
		PlayMontage(CombatMontage, "Hit_01", "Hit_02");
	}
}

void AEnemy::PlayHitSound()
{
	if(HitSound1 && HitSound2)
	{
		int32 RandSound = FMath::RandRange(0,2);
		switch (RandSound)
		{
			case 0:
				UGameplayStatics::PlaySound2D(this,HitSound1);
				break;
			case 2:
				UGameplayStatics::PlaySound2D(this,HitSound2);
				break;
			default:
				;
		}
	}
}

void AEnemy::PlayAttackSound()
{
	if(AttackSound1 && AttackSound2)
    	{
    		int32 RandSound = FMath::RandRange(0,2);
    		switch (RandSound)
    		{
    			case 0:
    				UGameplayStatics::PlaySound2D(this,AttackSound1);
    				break;
    			case 2:
    				UGameplayStatics::PlaySound2D(this,AttackSound2);
    				break;
    			default:
    				;
    		}
    	}
}

void AEnemy::PlayDeathSound()
{
if(Death1 && Death2)
	{
		int32 RandSound = FMath::RandRange(0,1);
		switch (RandSound)
		{
			case 0:
				UGameplayStatics::PlaySound2D(this,Death1);
				break;
			case 1:
				UGameplayStatics::PlaySound2D(this,Death2);
				break;
			default:
				;
		}
	}
}

void AEnemy::Disappear()
{
	GetWorldTimerManager().ClearTimer(AttackDelayTimer);
	EquipedWeapon->Destroy();
	Destroy();
	
}

void AEnemy::PlayScream()
{
	PlayMontage(CombatMontage, "Scream");
}

void AEnemy::PlayMontage(UAnimMontage* Montage, FName Section, float Speed)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if(AnimInstance && Montage)
	{
		AnimInstance->Montage_Play(Montage, Speed);
		AnimInstance->Montage_JumpToSection(Section);
	}
}

void AEnemy::PlayMontage(UAnimMontage* Montage, FName Section1, FName Section2)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	
	if(AnimInstance && Montage)
	{
		int32 Select = FMath::RandRange(0,1);
		switch(Select)
		{
		case 0:
			AnimInstance->Montage_Play(Montage, 1.f);
			AnimInstance->Montage_JumpToSection(Section1);
			break;
		case 1:
			AnimInstance->Montage_Play(Montage, 1.f);
			AnimInstance->Montage_JumpToSection(Section2);
			break;
		default:
			;
		}
	}
}

void AEnemy::PlayMontage(UAnimMontage* Montage, FName Section1, FName Section2, FName Section3)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	
	if(AnimInstance && Montage)
	{
		int32 Select = FMath::RandRange(0,2);
		switch(Select)
		{
		case 0:
			AnimInstance->Montage_Play(Montage, 1.f);
			AnimInstance->Montage_JumpToSection(Section1);
			break;
		case 1:
			AnimInstance->Montage_Play(Montage, 1.f);
			AnimInstance->Montage_JumpToSection(Section2);
			break;
		case 2:
			AnimInstance->Montage_Play(Montage, 1.f);
			AnimInstance->Montage_JumpToSection(Section3);
			break;
		default:
			;
		}
	}
}
