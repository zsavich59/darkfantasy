// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyBoss.h"

#include "MainCharacter.h"

AEnemyBoss::AEnemyBoss()
{
	bIsReady = false;
}

void AEnemyBoss::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void AEnemyBoss::AgroCollisionOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                             UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	PlayMontage(CombatMontage, "Rising");
	
	if(OtherActor)
	{
		AMainCharacter* Character = Cast<AMainCharacter>(OtherActor);
		if(Character)
		{
			SetTarget(Character);
		}
	}
}

void AEnemyBoss::CombatCollisionOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::CombatCollisionOnBeginOverlap(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep,
	                                     SweepResult);
}

void AEnemyBoss::Hit()
{
	
}
