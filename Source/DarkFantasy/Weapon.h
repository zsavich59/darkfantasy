// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NiagaraComponent.h"
#include "Weapon.generated.h"

UCLASS()
class DARKFANTASY_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon();

	// Components 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class USceneComponent* DefaultScene;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	class UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
    class UBoxComponent* CombatCollision;

	// Particles
	UPROPERTY(VisibleAnywhere, Category = "Combat | Particles")
	class UParticleSystemComponent* ExtraAttackParticle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat | Particles")
	UNiagaraSystem* HitNiagara;

	// Sounds
	UPROPERTY(EditDefaultsOnly, Category = "Combat | Sounds")
	class USoundCue* SwingSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat | Sounds")
	USoundCue* HitSound;

	// Params
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
	int32 MinDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
	int32 MaxDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Combat")
	bool bCanExtraParticles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat")
	TSubclassOf<UDamageType> DamageType;

	// Instigator
	UPROPERTY(VisibleAnywhere, Category = "Combat")
	class AController* WeaponInstigator;

	// Character
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat")
	class APawn* WeaponOwner;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat")
	AActor* DamagedActor;

	// VFX
	UPROPERTY(EditDefaultsOnly, Category = "VFX")
	UNiagaraComponent* NiagaraSword;

	UPROPERTY(EditDefaultsOnly, Category = "VFX")
	UMaterial* ShiningSwordMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Overlapped Functions
	UFUNCTION()
	void CombatBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void EquipWeapon(ACharacter* Character);

	UFUNCTION(BlueprintCallable)
	void ActivateCollision();
	UFUNCTION(BlueprintCallable)
	void DeactivateCollision();

	UFUNCTION(BlueprintCallable)
	void ActivateShining();

	UFUNCTION(BlueprintCallable)
	void PlayHitSounds();

	FORCEINLINE void SetInstigator(AController* Controller) {WeaponInstigator = Controller;}
};
