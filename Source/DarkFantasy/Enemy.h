// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"


UENUM(BlueprintType)
enum class EEnemyMovementStatus : uint8
{
	EMS_Patrol		UMETA(DisplayName = "Patrol"),
	EMS_MoveTo		UMETA(DisplayName = "MoveTo"),
	EMS_Attack		UMETA(DisplayName = "Attack"),
	EMS_Dead		UMETA(DisplayName = "Dead"),

	EMS_MAX			UMETA(DisplayName = "DefaultMAX")
};

UCLASS()
class DARKFANTASY_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AI")
	class AAIController* AI;

	// Collisions 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class USphereComponent* AgroCollision;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USphereComponent* CombatCollision;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	//class UBoxComponent* WeaponCollision;

	// Animations
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animations")
	class UAnimMontage* CombatMontage;

	// Sounds
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	class USoundCue* HitSound1;
	
	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	class USoundCue* HitSound2;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	class USoundCue* AttackSound1;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	class USoundCue* AttackSound2;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	class USoundCue* Death1;

	UPROPERTY(EditDefaultsOnly, Category = "Sounds")
	class USoundCue* Death2;

	// Combat Components
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Combat")
	TSubclassOf<class AWeapon> SelectEquipedWeapon;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Combat")
	AWeapon* EquipedWeapon;

	UPROPERTY(VisibleAnywhere, Category = "Combat")
	class AMainCharacter* Target;

	FTimerHandle AttackDelayTimer;

	// Enemy Stats
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stats")
	EEnemyMovementStatus MovementStatus;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats | Health")
	int32 Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats | Health")
	int32 MaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats | Damage")
	int32 MinDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats | Damage")
	int32 MaxDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats | Health")
	float MinAttackDelay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats | Damage")
	float MaxAttackDelay;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "Stats")
	bool bIsCriticalStatus;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Stats")
	bool bCanScream;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called when enemy get damage
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	// Overlapping Functions
	UFUNCTION()
	virtual void AgroCollisionOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION()
	void AgroCollisionOnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	virtual void CombatCollisionOnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION()
	void CombatCollisionOnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	// Movement Functions
	UFUNCTION(BlueprintCallable)
	void MoveToTarget();
	
	// Combat Actions Functional
	void SpawnWeapon();
	void Attack();
	UFUNCTION(BlueprintCallable)
	void AttackEnd();
	void LethalDie(FName AttackName);
	void Die();
	virtual void Hit();

	// Sounds
	UFUNCTION(BlueprintCallable)
	void PlayHitSound();
	UFUNCTION(BlueprintCallable)
	void PlayAttackSound();
	UFUNCTION(BlueprintCallable)
	void PlayDeathSound();
	
	UFUNCTION(BlueprintCallable)
	void Disappear();
	
	void PlayScream();

	// Combat Stats Functional
	void DecreaseHealth(float const Damage);

	// Play AnimMontages Function
	void PlayMontage(UAnimMontage* Montage, FName Section, float Speed = 1.f);
	void PlayMontage(UAnimMontage* Montage, FName Section1, FName Section2);
	void PlayMontage(UAnimMontage* Montage, FName Section1, FName Section2, FName Section3);

	FORCEINLINE void SetMovementStatus(EEnemyMovementStatus Status) {MovementStatus = Status;}
	FORCEINLINE void SetTarget(AMainCharacter* Actor) {Target = Actor;}
	FORCEINLINE void ClearTarget() {Target = nullptr;}
};
