// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MainCharacter.generated.h"

UENUM(BlueprintType)
enum class EMovementStatus : uint8
{
	EMS_Normal		UMETA(DisplayName="Normal"),
	EMS_Sprinting	UMETA(DisplayName="Sprinting"),
	EMS_Extra		UMETA(DisplayName="Extra"),
	EMS_Dead		UMETA(DisplayName="Dead"),

	EMS_MAX			UMETA(DisplayName="DefaultMAX")
};

UENUM(BlueprintType)
enum class EStaminaStatus : uint8
{
	ESS_Normal			UMETA(DisplayName="Normal"), // 100% - 30%
	ESS_Exhausted		UMETA(DisplayName="Exhausted"), // 0%
	ESS_Recovering		UMETA(DisplayName="Recovering"), // 0% - 30%

	ESS_MAX				UMETA(DisplayName="DefaultMAX")
};

UCLASS()
class DARKFANTASY_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()
	
	/** Camera boom positioning of camera behind the player */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "ture"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "ture"))
	class UCameraComponent* FollowCamera;

	// Character Stats
	float Health;
	float MaxHealth;

	float Stamina;
	float MaxStamina;
	float MinBorderStamina;

	float Energy;
	float MaxEnergy;
	
public:
	// Sets default values for this character's properties
	AMainCharacter();

	// Combat Props
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Combat")
	bool bIsAttacking;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Combat")
	bool bIsHitting;
	
	// Movement Props
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	EMovementStatus MovementStatus;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	EStaminaStatus StaminaStatus;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
	float NormalWalkSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Movement")
	float SprintingWalkSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	bool bIsTurnLeft;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	bool bIsTIP;
	
	bool bIsMoveForward;

	// Rates for Turn and LookUp
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	float BaseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	float BaseLookUpRate;

	// Set Camera Height value for Camera Boom Component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	float CameraHeight;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
	float CameraHeightMax;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
	float CameraHeightMin;

	bool bSmoothCameraHeight;

	// Jump Back Props
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "JumpBack")
	bool bSKeyJustUp;
	bool bShiftKeyDown;

	// Equiped Items
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
	class AWeapon* EquipedWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon");
	TSubclassOf<class AWeapon> SelectEquipedWeapon;

	// Animation Props
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
	class UAnimMontage* CombatMontage;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Functional for Movement */
	void MoveForward(float Value);
	void MoveRight(float Value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

	/** Key Down and Up handler */
	void ShiftKeyDown();
	void ShiftKeyUp();

	/** Functional for Camera */
	void UpdateCameraHeight(float Value);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	void SetMovementStatus(EMovementStatus Status);
	void SetStaminaStatus(EStaminaStatus Status);
	
	// Called when player enter double S to make fast jump back
	UFUNCTION(BlueprintCallable)
	void JumpBack();

	// Mouse Buttons Handlers
	void LeftMouseButton();
	void RightMouseButton();

	// Combat Functions
	void FastAttack();
	void LethalAttack();
	bool CanAttack();
	void Hit();
	void Die();

	// Health Functions
	void IncreaseHealth(float const Value);
	void DecreaseHealth(float const Value);
	
	// Stamina Functions
	void IncreaseStamina(float const Value);
	void DecreaseStamina(float const Value);

	// Energy Functions
	void IncreaseEnergy(float const Value);
	void DecreaseEnergy(float const Value);

	// Spawn Equipments Functions
	void SpawnWeapon();

	// Play AnimMontages Function
	void PlayMontage(UAnimMontage* Montage, FName Section, float Speed = 1.f);
	void PlayMontage(UAnimMontage* Montage, FName Section1, FName Section2);
	void PlayMontage(UAnimMontage* Montage, FName Section1, FName Section2, FName Section3);

	UFUNCTION(BlueprintCallable)
	void DeathEnd();
	
	// Combat Getters and Setters
	FORCEINLINE void SetEquipedWeapon(AWeapon* Weapon) {EquipedWeapon = Weapon;}

	// Character Stats Getters and Setters
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetHealth() {return Health;}
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetStamina() {return Stamina;}
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetEnergy() {return Energy;}

	FORCEINLINE void SetMaxHealth(float const Value) {MaxHealth = Value;}
	FORCEINLINE void SetMaxStamina(float const Value) {MaxStamina = Value;}
	FORCEINLINE void SetMaxEnergy(float const Value) {MaxEnergy = Value;}

	UFUNCTION(BlueprintCallable)
	void SetHealth(float const Value);
	
	UFUNCTION(BlueprintCallable)
	void SetStamina(float const Value);
	
	UFUNCTION(BlueprintCallable)
	void SetEnergy(float const Value);
};
