// Fill out your copyright notice in the Description page of Project Settings.


#include "MainAnimInstance.h"

#include "Enemy.h"
#include "MainCharacter.h"

void UMainAnimInstance::NativeInitializeAnimation()
{
	if(Pawn == nullptr)
	{
		Pawn = TryGetPawnOwner();
		if(Pawn)
		{
			Character = Cast<AMainCharacter>(Pawn);
			if(Character) return;

			Enemy = Cast<AEnemy>(Pawn);
			if(Enemy) return;
		}
	}
}

void UMainAnimInstance::UpdateAnimationProperties()
{
	if(Pawn == nullptr)
	{
		Pawn = TryGetPawnOwner();
	}
	
	if(Pawn)
	{
		// Find Movement Speed
		FVector Speed = Pawn->GetVelocity();
		FVector LateralSpeed = FVector(Speed.X, Speed.Y, 0.f);
		MovementSpeed = LateralSpeed.Size();

		// Find Direction
		Direction = CalculateDirection(Speed, Pawn->GetActorRotation());
	}
}