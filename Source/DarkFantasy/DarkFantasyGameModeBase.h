// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DarkFantasyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DARKFANTASY_API ADarkFantasyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
